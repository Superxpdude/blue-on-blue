"""Blue on Blue core files

All standard code should be placed within this folder, and be loaded by this file.
Any code specific to a single command or cog should be placed within that cog."""

from .bot import *
from . import checks
from . import views
